package eu.revevol;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import eu.revevol.GoogleCredentialProperty;

@Configuration
@PropertySource("classpath:google-credentials.properties")
public class PropertyAppContext {

	@Bean(name = "googleCredentialProperty")
	public GoogleCredentialProperty getDaoManager() {
		return new GoogleCredentialProperty();
	}

}
