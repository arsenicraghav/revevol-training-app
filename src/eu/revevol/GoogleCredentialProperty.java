package eu.revevol;

import org.springframework.core.env.Environment;

public class GoogleCredentialProperty {

	private Environment env;

	public String getApplicationName() {
		return env.getProperty("application.name");
	}

	public String getServiceAccountEmail() {
		return env.getProperty("service.account.email");
	}

	public String getServiceAccountId() {
		return env.getProperty("service.account.id");
	}

	public String getDriveScope() {
		return env.getProperty("drive.scope");
	}

	public String getKeyFilename() {
		return env.getProperty("p12file.name");
	}

	public GoogleCredentialProperty() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GoogleCredentialProperty(Environment env) {
		super();
		this.env = env;
	}

}
