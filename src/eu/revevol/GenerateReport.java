package eu.revevol;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

public class GenerateReport extends HttpServlet{
	
	public static final String PKCS12_INSTANCE = "PKCS12";
	public static final String SECRET_KEY = "notasecret";
	public static final String PRIVATE_KEY = "privatekey";
	public static final Long BUFFER_TIME = 28800000L;
	
    private static final String REPORT_FILE_NAME = "report.csv";
 
	
	private static final Logger logger = Logger.getLogger(GenerateReport.class.getName());
	
	private GoogleCredentialProperty googleCredentialProperty = new GoogleCredentialProperty();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			createReportFromServiceAccount();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}
	
	private Drive buildClient(String targetEmail) throws GeneralSecurityException, IOException {
		
		logger.info("Building drive API client for email: " + targetEmail);
		JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

		KeyStore keystore = KeyStore.getInstance(PKCS12_INSTANCE);
		keystore.load(this.getClass().getClassLoader().getResourceAsStream(googleCredentialProperty.getKeyFilename()), SECRET_KEY.toCharArray());

		PrivateKey pk = (PrivateKey) keystore.getKey(PRIVATE_KEY, SECRET_KEY.toCharArray());
		
		GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(JSON_FACTORY).setServiceAccountId(googleCredentialProperty.getServiceAccountEmail())
				.setServiceAccountPrivateKey(pk).setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE)).setServiceAccountUser(targetEmail).build();

		Drive service = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(googleCredentialProperty.getApplicationName()).build();

		logger.info("Building drive API client for email success.");
		return service;
	}

  
    public void createReportFromServiceAccount() throws IOException, GeneralSecurityException {

            Drive service = buildClient("training@revevolclod.com");

            FileList result = service.files().list()
                    .setFields("nextPageToken, files(id, name, fileExtension, permissions, fullFileExtension)")
                    .execute();
            
            List<File> items = result.getItems();
            
            for (File file : items) {
            	
            	System.out.println(file.getDownloadUrl());
            }
        }
}
